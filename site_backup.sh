#!/usr/bin/env bash

if [ $# -ne 2 ] # Check parameters
then
    echo -e "Please use: $0 <SITE_NAME> <DB_NAME>"
    exit 1
fi

### VARIABLES ###
KEEP_BACKUP=3
DATE=$(date "+%Y-%m-%d")
DATE_LOG=$(date "+%Y-%m-%d %H:%M:%S")
CLEAR_DATE=$(date -d "${DATE} ${KEEP_BACKUP} days ago" "+%Y-%m-%d")

SITE_NAME=$1
BACKUP_PATH="/var/www/BACKUP/${SITE_NAME}/${DATE}"
BACKUP_CLEAR="/var/www/BACKUP/${SITE_NAME}/${CLEAR_DATE}"
HOST_PATH="/var/www/html/"

DB_USER="root"
DB_PASS=""
DB_NAME=$2


### PROCESS ###
function backup
{
	### BackUP MySQL ###
	echo "${DATE_LOG} - Start BackUP MySQL process"

	mkdir -p "${BACKUP_PATH}"
	mysqldump -u"${DB_USER}" -p"${DB_PASS}" "${DB_NAME}" > "${BACKUP_PATH}/mysqldump_${DATE}.sql" &

	### BackUP site ###
	echo "${DATE_LOG} - Start BackUP files process"

	cp -r "${HOST_PATH}/${SITE_NAME}/" "${BACKUP_PATH}" &

	### CLEAR OLD BACKUPs ###
	echo "${DATE_LOG} - Start CLEANUP ${BACKUP_CLEAR} BackUP"

	rm -rf ${BACKUP_CLEAR} &

	echo "${DATE_LOG} - End BackUP process" 
}


backup 2>&1 | tee -a "${BACKUP_PATH}/backup_process.log"